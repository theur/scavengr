# Scavengr
Scavengr is an open-source, mobile-ready, web-based, Go powered game.

### Tech
Notable technologies used by Scavengr:
* [Echo](https://github.com/labstack/echo) - _"High performance, minimalist Go web framework"_
* [Xorm](https://github.com/go-xorm/xorm) - _"Simple and Powerful ORM for Go, ..."_
* [Lost](https://github.com/peterramsing/lost) - _(no, not ABC's lost) -  a grid system built in PostCSS that even works with vanilla CSS_

##### TODO: add more information

### Installation

Scavengr can be built on any platform that supports `go build`.
