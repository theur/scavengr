package main

import (
	"math/rand"
	"testing"

	"golang.org/x/crypto/bcrypt"
)

const letterBytes = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"

func RandStringBytes(n int) []byte {
	b := make([]byte, n)
	for i := range b {
		b[i] = letterBytes[rand.Intn(len(letterBytes))]
	}
	return b
}

func BenchmarkHashPassword8Cost(b *testing.B) {
	_benchmarkHashPasswordWithSalt(RandStringBytes(32), RandBytes(8), 8, b)
}

func BenchmarkHashPassword10Cost(b *testing.B) {
	_benchmarkHashPasswordWithSalt(RandStringBytes(32), RandBytes(8), 10, b)
}

func BenchmarkHashPassword12Cost(b *testing.B) {
	_benchmarkHashPasswordWithSalt(RandStringBytes(32), RandBytes(8), 12, b)
}

func _benchmarkHashPasswordWithSalt(password []byte, salt []byte, cost int, b *testing.B) {
	saltypassword := append(password, salt...)
	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		bcrypt.GenerateFromPassword(saltypassword, cost)
	}
}
