package main

import (
	"context"
	"flag"
	"fmt"
	"html/template"
	"io"
	"log"
	"math/rand"
	"os"
	"os/signal"
	"runtime/pprof"
	"time"

	"encoding/hex"

	"net/http"

	jwt "github.com/dgrijalva/jwt-go"
	"github.com/go-xorm/xorm"
	"github.com/labstack/echo"
	"github.com/labstack/echo/middleware"
	_ "github.com/mattn/go-sqlite3"
	"github.com/oxtoacart/bpool"
	"golang.org/x/crypto/bcrypt"
)

var (
	echoInstance         *echo.Echo
	templateBufferPool   *bpool.BufferPool
	serverPort           int
	serverDebugMode      bool
	dbFileName           string
	serverStartTime      time.Time
	db                   *xorm.Engine
	hmacSecretSessionKey []byte
	passwordHashCost     int
	cpuProfileFileName   string
)

/*
===============================================================================
	Database schema
===============================================================================
*/

// User .
type User struct {
	ID           int64
	Username     string
	PasswordSalt []byte `xorm:"blob(8)"`
	PasswordHash []byte `xorm:"blob(60)"`
	Email        string
	Created      time.Time `xorm:"created"`
	Updated      time.Time `xorm:"updated"`
}

/*
===============================================================================
	Session / security functions
===============================================================================
*/

// RandBytes generates a psuedo-random []byte of length n
func RandBytes(n int) []byte {
	b := make([]byte, n)
	for i := range b {
		b[i] = byte(rand.Intn(255))
	}
	return b
}

var letterChars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"

// RandLetters generates a psuedo-random letter-string of length n
func RandLetters(n int) string {
	b := make([]byte, n)
	for i := range b {
		b[i] = letterChars[rand.Intn(len(letterChars))]
	}
	return string(b)
}

func _fallbackGenerateSessionKey() []byte {
	log.Println("Missing or corrupt SCAVENGR_SESSION_KEY environment variable - sessions will not persist across server restarts")
	return RandBytes(64)
}

func configureSecureSessions() {
	_, found := os.LookupEnv("SCAVENGR_SESSION_KEY")
	if found {
		hmacSecretSessionKey, parseHexError := hex.DecodeString(os.Getenv("SCAVENGR_SESSION_KEY"))
		if parseHexError != nil || len(hmacSecretSessionKey) != 64 {
			hmacSecretSessionKey = _fallbackGenerateSessionKey()
		}
	} else {
		hmacSecretSessionKey = _fallbackGenerateSessionKey()
	}
}

func cookieFromUserObject(u *User) (*http.Cookie, error) {
	token := jwt.NewWithClaims(jwt.SigningMethodHS256, jwt.MapClaims{
		"User":    u.ID,
		"Expires": time.Now().Add(72 * time.Hour).Unix(),
	})
	tokenString, tokenSigningError := token.SignedString(hmacSecretSessionKey)
	if tokenSigningError != nil {
		return nil, tokenSigningError
	}
	cookie := new(http.Cookie)
	cookie.Name = "Session"
	cookie.Value = tokenString
	cookie.Expires = time.Now().Add(72 * time.Hour)
	return cookie, nil
}

func securityMiddleware(next echo.HandlerFunc) echo.HandlerFunc {
	return func(c echo.Context) error {
		c.Response().Header().Set(echo.HeaderXFrameOptions, "DENY")
		c.Response().Header().Set(echo.HeaderXContentTypeOptions, "nosniff")
		return next(c)
	}
}

func expireSession(ctx echo.Context) {
	cookie := new(http.Cookie)
	cookie.Name = "Session"
	cookie.Value = ""
	cookie.Expires = time.Unix(0, 0)
	ctx.SetCookie(cookie)
	ctx.Set("User", nil)
}

func authorisationMiddleware(next echo.HandlerFunc) echo.HandlerFunc {
	return func(c echo.Context) error {
		URI := c.Request().RequestURI
		// skip authorisation middleware if request is a static page
		if _, found := RouteMappingGet[URI]; !found {
			if _, found := RouteMappingPost[URI]; !found {
				return next(c)
			}
		}
		cookie, getCookieError := c.Request().Cookie("Session")
		if getCookieError != nil {
			return next(c)
		}
		if cookie != nil {
			token, tokenParseError := jwt.Parse(cookie.Value, func(tokenCtx *jwt.Token) (interface{}, error) {
				_, ok := tokenCtx.Method.(*jwt.SigningMethodHMAC)
				if !ok {
					return nil, fmt.Errorf("unexpected signing method %v", tokenCtx.Header["alg"])
				}
				return hmacSecretSessionKey, nil
			})
			if tokenParseError != nil {
				log.Println(fmt.Sprintf("parse session failure: %s", tokenParseError.Error()))
				expireSession(c)
				return next(c)
			}
			claims, claimsParseOk := token.Claims.(jwt.MapClaims)
			if !token.Valid {
				expireSession(c)
				return next(c)
			}
			if !claimsParseOk {
				expireSession(c)
				return next(c)
			}
			if claims["Expires"] == nil || claims["User"] == nil {
				fmt.Println(fmt.Errorf("missing expires / user key in auth token: %s", cookie.Value))
				expireSession(c)
				return next(c)
			}
			if int64(claims["Expires"].(float64)) < time.Now().Unix() {
				fmt.Println(fmt.Sprintf("expired token rejected: %s", cookie.Value))
				expireSession(c)
				return next(c)
			}
			if claims["User"] != nil {
				userID := int64(claims["User"].(float64))
				u := new(User)
				u.ID = userID
				found, err := db.Get(u)
				if err != nil {
					log.Println(fmt.Errorf("populate from token db exception: %s", err.Error()))
					expireSession(c)
					return next(c)
				}
				if !found {
					log.Println(fmt.Errorf("populate from token db failed: user ID %d not found", userID))
					expireSession(c)
					return next(c)
				}
				c.Set("User", u)
			}
		}
		return next(c)
	}
}

/*
===============================================================================
	HTTP handlers
===============================================================================
*/

// RouteMappingGet holds a route mapping for http GET requests to Go funcs
var RouteMappingGet = map[string]echo.HandlerFunc{
	"/":        onHTTPGetIndex,
	"/signin":  onHTTPGetSignin,
	"/signup":  onHTTPGetSignup,
	"/signout": onHTTPGetSignout,
}

// RouteMappingPost holds a route mapping for http POST requests to Go funcs
var RouteMappingPost = map[string]echo.HandlerFunc{
	"/signin": onHTTPPostSignin,
	"/signup": onHTTPPostSignup,
	"/":       onHTTPGetIndex,
}

func onHTTPGetSignout(ctx echo.Context) error {
	templateVariables := map[string]interface{}{}
	if ctx.Get("User") == nil {
		return ctx.Redirect(http.StatusSeeOther, "/")
	}
	expireSession(ctx)
	return TemplateResponse(ctx, 200, "signout", templateVariables)
}

func onHTTPGetIndex(ctx echo.Context) error {
	templateVariables := map[string]interface{}{}
	return TemplateResponse(ctx, 200, "index", templateVariables)
}

func onHTTPGetSignin(ctx echo.Context) error {
	templateVariables := map[string]interface{}{}
	if ctx.Get("User") != nil {
		return ctx.Redirect(http.StatusSeeOther, "/")
	}
	return TemplateResponse(ctx, 200, "signin", templateVariables)
}

func onHTTPPostSignin(ctx echo.Context) error {
	templateVariables := map[string]interface{}{}
	if ctx.Get("User") != nil {
		return ctx.Redirect(http.StatusSeeOther, "/")
	}
	reqData := new(struct {
		Username string `form:"username"`
		Password string `form:"password"`
	})
	err := ctx.Bind(reqData)
	if err != nil {
		log.Println(fmt.Errorf("signin exception: %s", err.Error()))
		return TemplateResponse(ctx, 400, "signin", map[string]interface{}{"ErrorMessage": "Invalid sign in request"})
	}
	u := new(User)
	u.Username = reqData.Username
	found, err := db.Get(u)
	if err != nil {
		log.Println(fmt.Errorf("signin exception: %s", err.Error()))
		return TemplateResponse(ctx, 400, "signin", map[string]interface{}{"ErrorMessage": "Invalid sign in request"})
	}
	if !found {
		templateVariables["ErrorMessage"] = "Wrong username / password"
		return TemplateResponse(ctx, 200, "signin", templateVariables)
	}
	saltypassword := append([]byte(reqData.Password), u.PasswordSalt...)
	compareHashError := bcrypt.CompareHashAndPassword(u.PasswordHash, saltypassword)
	if compareHashError != nil {
		templateVariables["ErrorMessage"] = "Wrong username / password"
		return TemplateResponse(ctx, 200, "signin", templateVariables)
	}
	sessionCookie, createCookieError := cookieFromUserObject(u)
	if createCookieError != nil {
		log.Println(fmt.Errorf("sign and create cookie error: %s", createCookieError.Error()))
	} else {
		ctx.SetCookie(sessionCookie)
	}
	//ctx.Set("User", u)
	return ctx.Redirect(http.StatusSeeOther, "/")
}

func onHTTPGetSignup(ctx echo.Context) error {
	templateVariables := map[string]interface{}{}
	return TemplateResponse(ctx, 200, "signup", templateVariables)
}

func onHTTPPostSignup(ctx echo.Context) error {
	templateVariables := map[string]interface{}{}
	reqData := new(struct {
		Username string `form:"username"`
		Email    string `form:"email"`
		Password string `form:"password"`
	})
	err := ctx.Bind(reqData)
	if err != nil {
		log.Println(fmt.Errorf("signup exception: %s", err.Error()))
		return ErrorResponse(ctx, 500, "Invalid sign up request")
	}
	u := new(User)
	u.Username = reqData.Username
	found, _ := db.Get(u)
	if found {
		templateVariables["ErrorMessage"] = "That username has already been taken"
		return TemplateResponse(ctx, 200, "signup", templateVariables)
	}
	salt := RandBytes(8)
	saltypassword := append([]byte(reqData.Password), salt...)
	passwordHash, computeHashError := bcrypt.GenerateFromPassword(saltypassword, passwordHashCost)
	if computeHashError != nil {
		log.Println(fmt.Errorf("signup generate hash exception: %s", err.Error()))
		return ErrorResponse(ctx, 500, "Invalid sign up request")
	}
	// sanity checks on hash:
	if len(passwordHash) != 60 {
		log.Println(fmt.Errorf("sanity hash check failed: hash length=%d, need=60", len(passwordHash)))
		return ErrorResponse(ctx, 500, "Invalid sign up request")
	}
	allEqual := true
	for i := 1; i < 59; i++ {
		if passwordHash[i] != passwordHash[0] {
			allEqual = false
		}
	}
	if allEqual {
		log.Println(fmt.Errorf("sanity hash check failed: all bytes equal %#x", passwordHash[0]))
		return ErrorResponse(ctx, 500, "Invalid sign up request")
	}

	u.Email = reqData.Email
	u.PasswordHash = passwordHash
	u.PasswordSalt = salt
	u.Username = reqData.Username
	_, dbInsertError := db.InsertOne(u)
	if dbInsertError != nil {
		log.Println(fmt.Errorf("failed to create user: %s", dbInsertError))
		return TemplateResponse(ctx, 400, "signup", map[string]interface{}{"ErrorMessage": "Invalid sign up request"})
	}
	ctx.Set("User", u)
	return onHTTPGetIndex(ctx)
}

func onHTTPError(err error, ctx echo.Context) {
	code := 500
	msg := "There's an internal server issue. Please try again later."
	if he, ok := err.(*echo.HTTPError); ok {
		code = he.Code
		if code == http.StatusNotFound {
			msg = "The requested file could not be found"
		} else if code == http.StatusForbidden {
			msg = "You do not have permission to view this page."
			if ctx.Get("User") == nil {
				msg += " You must be signed in."
			}
		}
	} else if serverDebugMode || (ctx.Get("User") != nil && ctx.Get("User").(*User).Email == "leo@localhost") {
		msg = err.Error()
	}
	log.Println(fmt.Errorf("http error code %d (method %s): %s", code, ctx.Request().Method, err.Error()))
	if !ctx.Response().Committed {
		if ctx.Request().Method == http.MethodHead {
			log.Println("HEAD received")
			ctx.NoContent(code)
		} else {
			ErrorResponse(ctx, code, msg)
		}
	}
}

// TemplateResponse wraps a call to render a template, adding useful common variables
func TemplateResponse(ctx echo.Context, statusCode int, templateName string, templateContent map[string]interface{}) error {
	templateContent["User"] = ctx.Get("User")
	return ctx.Render(statusCode, templateName, templateContent)
}

// ErrorResponse provides a convenience wrapper to returning an error response to user
func ErrorResponse(ctx echo.Context, statusCode int, errorString string) error {
	return TemplateResponse(ctx, statusCode, "error", map[string]interface{}{"ErrorMessage": errorString})
}

/*
===============================================================================
	In-template functions
===============================================================================
*/

var baseTemplateFunctions = template.FuncMap{
	"ServerTimeNow": serverTimeNow,
	"ServerUptime":  serverUptime,
}

func serverTimeNow() string {
	return time.Now().String()
}

func serverUptime() string {
	return time.Now().Sub(serverStartTime).String()
}

/*
===============================================================================
	Server initialisation
===============================================================================
*/

var TemplateMapping = map[string]string{
	"index":   "templates/index.tmpl",
	"signin":  "templates/signin.tmpl",
	"signup":  "templates/signup.tmpl",
	"signout": "templates/signout.tmpl",
	"error":   "templates/error.tmpl",
}

func loadTemplates() {
	templateBufferPool = bpool.NewBufferPool(32)
	templatesArray := &nativeTemplatesStruct{
		nativeTemplates: map[string]*template.Template{},
	}
	for templateName, templateFilePath := range TemplateMapping {
		templatesArray.nativeTemplates[templateName] = template.Must(template.New(templateName).Funcs(baseTemplateFunctions).ParseFiles("templates/base.tmpl", templateFilePath))
	}
	echoInstance.Renderer = templatesArray
}

func configureORM() {
	dbXorm, err := xorm.NewEngine("sqlite3", dbFileName)
	if err != nil {
		panic(err)
	}
	dbXorm.Sync2(new(User))
	db = dbXorm
}

func configureEchoServer() {
	echoInstance = echo.New()
	for requestPath, internalFunc := range RouteMappingGet {
		echoInstance.GET(requestPath, internalFunc)
	}
	for requestPath, internalFunc := range RouteMappingPost {
		echoInstance.POST(requestPath, internalFunc)
	}
	echoInstance.Static("/static", "static/")
	echoInstance.Debug = serverDebugMode
	echoInstance.Use(middleware.LoggerWithConfig(middleware.LoggerConfig{
		Format: "${method} ${uri} - respond with ${status}\n"}))
	echoInstance.Use(middleware.Recover())
	echoInstance.Use(authorisationMiddleware)
	echoInstance.Use(securityMiddleware)
	echoInstance.HTTPErrorHandler = onHTTPError
	//echoInstance.Use(middleware.GzipWithConfig(middleware.GzipConfig{Level: 9}))
}

func main() {
	flag.IntVar(&serverPort, "port", 3000, "port for the web server")
	flag.IntVar(&passwordHashCost, "passwordhashcost", 10, "bcrypt password hashing rounds (2^n)")
	flag.BoolVar(&serverDebugMode, "debug", false, "start server in debug mode (WARNING: unsafe)")
	flag.StringVar(&dbFileName, "db", "db.sqlite3", "specify a database file to use (sqlite3)")
	flag.StringVar(&cpuProfileFileName, "cpuprofile", "", "specify a file to output CPU benchmark information")
	flag.Parse()
	if cpuProfileFileName != "" {
		f, err := os.Create(cpuProfileFileName)
		if err != nil {
			log.Fatal(err)
		}
		pprof.StartCPUProfile(f)
	}
	configureSecureSessions()
	configureORM()
	configureEchoServer()
	loadTemplates()
	go func() {
		echoInstance.Logger.Fatal(echoInstance.Start(fmt.Sprintf(":%d", serverPort)))
	}()
	serverStartTime = time.Now()
	exit := make(chan os.Signal)
	signal.Notify(exit, os.Interrupt)
	onExit(<-exit)
}

func onExit(sig os.Signal) {
	ctx, cancel := context.WithTimeout(context.Background(), 3*time.Second)
	if cpuProfileFileName != "" {
		pprof.StopCPUProfile()
		log.Println("Stored CPU profile into " + cpuProfileFileName)
	}
	defer cancel()
	if err := echoInstance.Shutdown(ctx); err != nil {
		echoInstance.Logger.Fatal(err)
	}
}

/*
===============================================================================
	Miscelaneous functions
===============================================================================
*/

// nativeTemplatesStruct holds a hashmap of 'html/template' templates
type nativeTemplatesStruct struct {
	nativeTemplates map[string]*template.Template
}

// Render will override Echo's default template render logic
func (t *nativeTemplatesStruct) Render(w io.Writer, templateName string, data interface{},
	c echo.Context) error {
	buf := templateBufferPool.Get()
	defer templateBufferPool.Put(buf)
	desiredTemplate, ok := t.nativeTemplates[templateName]
	if !ok {
		// something went wrong
		return fmt.Errorf("the template %s does not exist", templateName)
	}
	err := desiredTemplate.ExecuteTemplate(buf, "base.tmpl", data)
	if err != nil {
		return fmt.Errorf("template parsing failed: %s", err.Error())
	}
	buf.WriteTo(w)
	return nil
}
